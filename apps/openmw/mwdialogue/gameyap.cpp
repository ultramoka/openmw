#include "gameyap.hpp"
#include "../mwbase/dialoguemanager.hpp"
#include "../mwbase/environment.hpp"
#include "../mwbase/windowmanager.hpp"
#include "../mwbase/mechanicsmanager.hpp"
#include "../mwgui/referenceinterface.hpp"
#include "../mwgui/dialogue.hpp"
#include "../mwworld/class.hpp"
#include "../mwmechanics/npcstats.hpp"
#include "../mwworld/cellstore.hpp"
#include "../mwbase/environment.hpp"
#include "../mwbase/world.hpp"
#include "../mwmechanics/actorutil.hpp"

#include "../../deps/cpp-httplib/httplib.h"
#include "../../deps/json/json.hpp"
#include <string>
#include <MyGUI_Delegate.h>

const GameYapResponse GameYap::sendRequest(MWWorld::Ptr& mPtr, const std::string& text, MWBase::DialogueManager::ResponseCallback* winCallback)
{    
    httplib::Client cli("localhost", 3000);
    nlohmann::json requestBody;
    nlohmann::json responseBody;
    GameYapResponse gyResp;
    std::string query = "/game/openmw/classify";
    int y, m, d, weather;
    std::stringstream datetime;
    MWWorld::TimeStamp ts;
    MWWorld::CellStore* cell;
    std::string jsonStr;
    MWBase::World* world = MWBase::Environment::get().getWorld();

    // collect context info (npc, location, weather, datetime)
    y = world->getYear();
    m = world->getMonth();
    d = world->getDay();
    weather = world->getCurrentWeather();

    ts = world->getTimeStamp();

    datetime << y << "-" << m << "-" << d << " " << ts.getHour() << "00";

    cell = MWMechanics::getPlayer().getCell();

    requestBody["statement"] = text;
    requestBody["npc"] = mPtr.getClass().getName(mPtr);
    requestBody["location"] = world->getCellName(cell);
    requestBody["datetime"] = datetime.str();
    requestBody["weather"] = weather;


    jsonStr = requestBody.dump();

    //MWBase::Environment::get().getWindowManager()->messageBox(jsonStr);

    auto res = cli.Post(query.c_str(), jsonStr, "application/json");
    if (res && res->status == 200) {
        responseBody = nlohmann::json::parse(res->body);
        gyResp.topic = responseBody["topic"].get<std::string>();
    }
    else {        
        gyResp.error = "HTTP_REQUEST_ERROR";
        gyResp.errorMessage = "GameYap server request failed: HTTP status " + res->status;
    }

    //MWBase::Environment::get().getWindowManager()->messageBox(res->body);

    processResponse(mPtr, gyResp, winCallback);
    return gyResp;
}


const void GameYap::processResponse(MWWorld::Ptr& mPtr, GameYapResponse& gyResp, MWBase::DialogueManager::ResponseCallback* winCallback)
{
    // do disposition change
    if (gyResp.dispositionDelta > 0) {
        MWMechanics::NpcStats& npcStats = mPtr.getClass().getNpcStats(mPtr);
        npcStats.setBaseDisposition(static_cast<int>(npcStats.getBaseDisposition() + gyResp.dispositionDelta));
    }

    // ----------------------------------------------------------
    // handle custom topics
    if (gyResp.topic == "greeting")
    {
        gyResp.message1 = "Hello";
        gyResp.topic = "";
    }
    else if (gyResp.topic == "goodbye")
    {
        MWBase::Environment::get().getDialogueManager()->goodbyeSelected();
        MWBase::Environment::get().getWindowManager()->removeGuiMode(MWGui::GM_Dialogue);
        // MWGui::resetReference():
        mPtr = MWWorld::Ptr();
    }
    else if (gyResp.topic == "where am i")
    {
        // display the name of the cell, then redirect to the "specific place" topic
        MWWorld::CellStore* cell = MWMechanics::getPlayer().getCell();
        std::string cellName = MWBase::Environment::get().getWorld()->getCellName(cell);        
        gyResp.message1 = cellName;
        gyResp.topic = "specific place";
    }
    else if (gyResp.topic == "who are you")
    {
        // display the name of the NPC, then
        // if it's a generic character, redirect to the "my trade" topic
        // else redirect to the "my background" topic
        std::string npcName = mPtr.getClass().getName(mPtr);        
        gyResp.message1 = npcName;
        gyResp.topic = "my trade";
    }
    else if (gyResp.topic == "persuasion_bribe10") {
        MWBase::Environment::get().getDialogueManager()->persuade(MWBase::MechanicsManager::PT_Bribe10, winCallback);
        gyResp.topic = "";
    }

    else if (gyResp.topic == "persuasion_bribe100")
    {
        MWBase::Environment::get().getDialogueManager()->persuade(MWBase::MechanicsManager::PT_Bribe100, winCallback);
        gyResp.topic = "";
    }
    else if (gyResp.topic == "persuasion_bribe1000")
    {
        MWBase::Environment::get().getDialogueManager()->persuade(MWBase::MechanicsManager::PT_Bribe1000, winCallback);
        gyResp.topic = "";
    }

    else if (gyResp.topic == "persuasion_admire")
    {
        MWBase::Environment::get().getDialogueManager()->persuade(MWBase::MechanicsManager::PT_Admire, winCallback);
        gyResp.topic = "";
    }

    else if (gyResp.topic == "persuasion_intimidate")
    {
        MWBase::Environment::get().getDialogueManager()->persuade(MWBase::MechanicsManager::PT_Intimidate, winCallback);
        gyResp.topic = "";
    }

    else if (gyResp.topic == "persuasion_taunt")
    {
        MWBase::Environment::get().getDialogueManager()->persuade(MWBase::MechanicsManager::PT_Taunt, winCallback);
        gyResp.topic = "";
    }
}

