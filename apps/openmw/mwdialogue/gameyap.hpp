#pragma once
#include "../mwbase/dialoguemanager.hpp"
#include <string>
#include "../mwgui/dialogue.hpp"

struct GameYapResponse {
    std::string message1 = "";
    std::string topic = "";
    std::string message2 = "";
    int dispositionDelta = 0;
    std::string error = "";
    std::string errorMessage = "";
};

class GameYap
{
public:
    static const GameYapResponse sendRequest(MWWorld::Ptr& mPtr, const std::string& text, MWBase::DialogueManager::ResponseCallback* winCallback);
    static const void processResponse(MWWorld::Ptr& mPtr, GameYapResponse& gyResp, MWBase::DialogueManager::ResponseCallback* winCallback);
};

